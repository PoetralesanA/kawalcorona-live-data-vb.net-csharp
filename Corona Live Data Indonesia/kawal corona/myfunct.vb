﻿'--------------------------------------------
'              ./Info                        ;
' Module + App Develop by PoetralesanA       ;
' Thanks : Teguh Aprianto                    ;
'                                            ;
'                                            ;
' Donate for my Source Code ?                ;
' Contact : Poetralesana.vbproj@gmail.com    ;
' Phone : 085656219405                       ;
'---------------------------------------------
Imports Excel = Microsoft.Office.Interop.Excel
Module myfunct
    Public Sub visitLink(ByVal mylink, ByVal title)
        If MessageBox.Show("Visit Link : " & vbNewLine & mylink, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Process.Start(mylink)
        End If
    End Sub
    Public Sub PropertiesDatagird(ByVal Grid As DataGridView)
        With Grid
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .MultiSelect = False
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .ReadOnly = True
        End With
    End Sub
    Private Declare Function GetWindow Lib "user32.dll" (ByVal hwnd As Integer, ByVal wCmd As Integer) As Integer
    Private Declare Auto Function SendMessageString Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As String) As Integer
    Const GW_CHILD = 9
    Const EM_SETCUEBANNER = &H1501
    Public Sub MyPlaceHoldr(ByVal Ctl As TextBox, ByVal Txt As String)
        SendMessageString(Ctl.Handle, EM_SETCUEBANNER, 1, Txt)
    End Sub
    Public Sub DisableWrite(ByVal e As KeyPressEventArgs)
        e.Handled = True
    End Sub
    Public Sub ShowAllItemCombobox(ByVal combobox As ComboBox)
        combobox.DroppedDown = True
    End Sub
    ' Menghitung totalRows(integer) dalam gird, sesuai column(index) yang telah ditentukan
    Public Sub SearchDt(ByVal Text As String, ByVal DatagridData As DataGridView)
        Try
            Dim getRow As DataGridViewRow = (
            From row As DataGridViewRow In DatagridData.Rows
            From c As DataGridViewCell In row.Cells
            Where (LCase(c.FormattedValue)).ToString.Contains(LCase(Text)) Select row
            ).First

            DatagridData.Rows(getRow.Index).Selected = True
        Catch ex As Exception
            MsgBox(IIf(ex.Message.Contains("Sequence"), "Data tidak ditemukan", ex.Message), MsgBoxStyle.Exclamation, "")
        End Try

    End Sub
    Function Enter(ByVal e As KeyPressEventArgs) As Boolean
        If Asc(e.KeyChar) = Keys.Enter Then
            e.Handled = True
            Return True
        Else
            Return False
        End If
    End Function
#Region "Export Datagrid > Excel"
    'Hello vb.net information,.. ARIGATOU :
    '- http://vb.net-informations.com/datagridview/vb.net_datagridview_export.htm
    Public Sub ExportDGV_Excel(ByVal mydatagrid)
      
        Using saveFileDialog As New SaveFileDialog
            saveFileDialog.InitialDirectory = Application.StartupPath
            Dim path As String = String.Empty
            Dim name As String = String.Empty


            saveFileDialog.FileName = "Corona Live Data by PoetralesanA"
            saveFileDialog.Filter = "|*.xls"

            '//Openfile Lokasi
            Dim result As DialogResult = saveFileDialog.ShowDialog()
         

            If Not result = DialogResult.Cancel Then
                '//GetPath
                path = System.IO.Path.GetDirectoryName(saveFileDialog.FileName)
                '//GetNameFile
                name = System.IO.Path.GetFileName(saveFileDialog.FileName)


                Try
                    Dim xlApp As Excel.Application
                    Dim xlWorkBook As Excel.Workbook
                    Dim xlWorkSheet As Excel.Worksheet
                    Dim misValue As Object = System.Reflection.Missing.Value

                    Dim i As Int16, j As Int16

                    xlApp = New Excel.Application
                    xlWorkBook = xlApp.Workbooks.Add(misValue)
                    xlWorkSheet = xlWorkBook.Sheets("sheet1")


                    For i = 0 To mydatagrid.RowCount - 1
                        For j = 0 To mydatagrid.ColumnCount - 1
                            xlWorkSheet.Cells(i + 1, j + 1) = mydatagrid(j, i).Value.ToString()
                        Next
                    Next

                    xlWorkBook.SaveAs(path & "\" & name, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, _
                     Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
                    xlWorkBook.Close(True, misValue, misValue)
                    xlApp.Quit()

                    releaseObject(xlWorkSheet)
                    releaseObject(xlWorkBook)
                    releaseObject(xlApp)
                    MessageBox.Show("FILE EXPORT~" & vbNewLine & vbNewLine &
                                    "Location : " & path & vbNewLine &
                                    "Name File : " & name,
                                    "[FINISH]", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End Using
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
            MessageBox.Show("Exception Occured while releasing object " + ex.ToString())
        Finally
            GC.Collect()
        End Try
    End Sub
#End Region
End Module

Class ClassCalculate

    Function calculategrid(ByVal DataGrid As DataGridView, ByVal CellIndex As Integer)
        'Dim TMPint As Integer = 0
        'Try
        '    For DataRows As Integer = 0 To DataGrid.Rows.Count - 1
        '        TMPint += Val(DataGrid.Rows(DataRows).Cells(CellIndex).Value)
        '    Next
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Kesalahan dalam pemilihan Column")
        '    Application.Exit()
        'End Try

        Dim TMPint As Integer = 0
        Try
            For DataRows As Integer = 0 To DataGrid.Rows.Count - 1
                If TypeOf DataGrid.Rows(DataRows).Cells(CellIndex).Value Is DBNull = False Then
                    TMPint += Convert.ToDouble(DataGrid.Rows(DataRows).Cells(CellIndex).Value)
                End If
                'TMPint += Val(DataGrid.Rows(DataRows).Cells(CellIndex).Value)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Kesalahan dalam pemilihan Column")
            Application.Exit()
        End Try

        Return TMPint
    End Function
End Class