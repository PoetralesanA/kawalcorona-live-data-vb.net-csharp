﻿'--------------------------------------------
'              ./Info                        ;
' Module + App Develop by PoetralesanA       ;
' Thanks : Teguh Aprianto                    ;
'                                            ;
'                                            ;
' Donate for my Source Code ?                ;
' Contact : Poetralesana.vbproj@gmail.com    ;
' Phone : 085656219405                       ;
'---------------------------------------------
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Namespace kawalcorona
#Region "Data Indonesia"
    Module indonesia
        Public Function indonesia() As DataTable
            '//Vaiable
            Dim _Name, _Positif, _Sembuh, _Meninggal, _Dirawat As String


            Using dt As New DataTable
                '//Column
                dt.Columns.Add("Name")
                dt.Columns.Add("Positif")
                dt.Columns.Add("Sembuh")
                dt.Columns.Add("Meninggal")
                dt.Columns.Add("Dirawat")


                Using webClient As New System.Net.WebClient
                    '//API_LINK + GetSource
                    Dim result As String = webClient.DownloadString("https://api.kawalcorona.com/indonesia")
                    '//Parse Data
                    Dim JsonArray As Linq.JArray = Linq.JArray.Parse(result)

                    '//Get String Data 
                    For Each Jsontoken As JToken In JsonArray
                        _Name = Jsontoken.SelectToken("name")
                        _Positif = Jsontoken.SelectToken("positif")
                        _Sembuh = Jsontoken.SelectToken("sembuh")
                        _Meninggal = Jsontoken.SelectToken("meninggal")
                        _Dirawat = Jsontoken.SelectToken("dirawat")
                        '//Add Data To Rows
                        dt.Rows.Add(_Name, _Positif, _Sembuh, _Meninggal, _Dirawat)
                    Next
                End Using
                Return dt
            End Using
        End Function
        Public Function provinsi() As DataTable
            '//Vaiable
            Dim _Provinsi, _Positif, _Sembuh, _Meninggal As String


            Using dt As New DataTable
                '//Column
                dt.Columns.Add("Provinsi")
                dt.Columns.Add("Positif")
                dt.Columns.Add("Sembuh")
                dt.Columns.Add("Meninggal")


                Using webClient As New System.Net.WebClient
                    '//API_LINK + GetSource
                    Dim result As String = webClient.DownloadString("https://api.kawalcorona.com/indonesia/provinsi")
                    '//Parse Data
                    Dim JsonArray As Linq.JArray = Linq.JArray.Parse(result)

                    '//Get String Data
                    For Each Jsontoken As JToken In JsonArray
                        _Provinsi = Jsontoken.SelectToken("attributes.Provinsi")
                        _Positif = Jsontoken.SelectToken("attributes.Kasus_Posi")
                        _Sembuh = Jsontoken.SelectToken("attributes.Kasus_Semb")
                        _Meninggal = Jsontoken.SelectToken("attributes.Kasus_Meni")

                        '//Add Data To Rows
                        dt.Rows.Add(_Provinsi, _Positif, _Sembuh, _Meninggal)
                    Next
                End Using
                Return dt
            End Using
        End Function
    End Module
#End Region
    '/////////////////////////////////////////////////////////////////////////////////
    '////////////////////////////////////////////////////////////////////////////////
    '///////////////////////////////////////////////////////////////////////////////
#Region "Data Global"
    Module global_
        Public Function Globaldata() As DataTable
            '//Vaiable
            Dim _region,
                _lastupdate,
                _lat,
                _long,
                _confirm,
                _death,
                _recovered,
                _actives As String


            Using dt As New DataTable
                '//Column
                dt.Columns.Add("Country")
                dt.Columns.Add("Last Update")
                dt.Columns.Add("Lat")
                dt.Columns.Add("Long")
                dt.Columns.Add("Confirmed")
                dt.Columns.Add("Death")
                dt.Columns.Add("Recovered")
                dt.Columns.Add("Actives")

                Using webClient As New System.Net.WebClient
                    '//API_LINK + GetSource
                    Dim result As String = webClient.DownloadString("https://api.kawalcorona.com/")
                    '//Parse Data
                    Dim JsonArray As Linq.JArray = Linq.JArray.Parse(result)

                    '//Get String Data
                    For Each Jsontoken As JToken In JsonArray
                        _region = Jsontoken.SelectToken("attributes.Country_Region")
                        _lastupdate = Jsontoken.SelectToken("attributes.Last_Update")
                        _lat = Jsontoken.SelectToken("attributes.Lat")
                        _long = Jsontoken.SelectToken("attributes.Long_")
                        _confirm = Jsontoken.SelectToken("attributes.Confirmed")
                        _death = Jsontoken.SelectToken("attributes.Deaths")
                        _recovered = Jsontoken.SelectToken("attributes.Recovered")
                        _actives = Jsontoken.SelectToken("attributes.Active")
                        '//Add Data To Rows
                        dt.Rows.Add(_region, _lastupdate, _lat, _long, _confirm, _death, _recovered, _actives)
                    Next
                End Using
                Return dt
            End Using
        End Function
        Public Function Positif() As DataTable
            '//Vaiable
            Dim _name, _value As String

            Using dt As New DataTable
                '//Column
                dt.Columns.Add("Total Positif")


                Using webClient As New System.Net.WebClient
                    '//API_LINK + GetSource
                    Dim result As String = webClient.DownloadString("https://api.kawalcorona.com/sembuh")
                    '//Parse Data
                    Dim JsonArray As Linq.JArray = Linq.JArray.Parse(result)

                    '//Get String Data
                    For Each Jsontoken As JToken In JsonArray
                        _name = Jsontoken.SelectToken("name")
                        _value = Jsontoken.SelectToken("value")
                        '//Add Data To Rows
                        dt.Rows.Add(_name, _value)
                    Next
                End Using
                Return dt
            End Using
        End Function
    End Module
#End Region
End Namespace