﻿Public Class kuli
    Private Sub dev_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        vr.Text = "Version : " & Application.ProductVersion
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
        Label1.ForeColor = Color.Yellow
        Me.Close()
    End Sub

    Private Sub Label1_MouseHover(sender As Object, e As EventArgs) Handles Label1.MouseHover
        Label1.ForeColor = Color.Aquamarine
    End Sub

    Private Sub Label1_MouseLeave(sender As Object, e As EventArgs) Handles Label1.MouseLeave
        Label1.ForeColor = Color.Red
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Try
            myfunct.visitLink("https://www.facebook.com/poetralesana", "[Facebook]")
        Catch ex As Exception
            '//Handle Error Jika tidak memiliki browser alternatif
            Process.Start("iexplore", "www.facebook.com/poetralesana")
        End Try
    End Sub

    Private Sub dev_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Try
            Process.Start("https://www.youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ")
        Catch ex As Exception
            '//Handle Error Jika tidak memiliki browser alternatif
            Process.Start("iexplore", "https://www.youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ")
        End Try
    End Sub
End Class