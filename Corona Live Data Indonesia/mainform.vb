﻿'--------------------------------------------
'              ./Info                        ;
' Module + App Develop by PoetralesanA       ;
' Thanks : Teguh Aprianto                    ;
'                                            ;
'                                            ;
' Donate for my Source Code ?                ;
' Contact : Poetralesana.vbproj@gmail.com    ;
' Phone : 085656219405                       ;
'---------------------------------------------
Public Class mainform
    Dim totalpositif As New ClassCalculate
    Dim totalsembuh As New ClassCalculate
    Dim totalmeninggal As New ClassCalculate

    Private Sub API_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0 'Indonesia

        myfunct.PropertiesDatagird(DataGridView1) 'properties dgv
        myfunct.MyPlaceHoldr(TextBox1, "Cari Data...") 'placeholder search
        DataGridView1.DataSource = kawalcorona.global_.Globaldata 'View Data



        calculatedataglobal()
    End Sub

    Private Sub ComboBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox1.KeyPress
        myfunct.DisableWrite(e)
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Select Case ComboBox1.SelectedIndex
            Case 1 'Indonesia
                DataGridView1.DataSource = kawalcorona.indonesia.indonesia
            Case 2 'Provinsi
                DataGridView1.DataSource = kawalcorona.indonesia.provinsi
            Case 3 'Data Global
                DataGridView1.DataSource = kawalcorona.global_.Globaldata
                calculatedataglobal()
                'DataGridView1.DataSource = kawalcorona.global_.Positif
        End Select
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        myfunct.visitLink("https://kawalcorona.com/api/", "Kawal Corona Api")
    End Sub

    Private Sub ComboBox1_MouseClick(sender As Object, e As MouseEventArgs) Handles ComboBox1.MouseClick
        myfunct.ShowAllItemCombobox(ComboBox1)
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        Select Case myfunct.Enter(e)
            Case True
                myfunct.SearchDt(TextBox1.Text, DataGridView1)
        End Select
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        kuli.ShowDialog()
    End Sub


    Private Sub ExportToExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportToExcelToolStripMenuItem.Click
        myfunct.ExportDGV_Excel(DataGridView1)
    End Sub
#Region "DataView"
    Private Function viewIndo()
        Using loadDT As New DataTable
            loadDT.Merge(kawalcorona.indonesia.indonesia)
            Return "Positif : " & loadDT.Rows(0).Item(1) & " Jiwa" & vbNewLine &
                   "Sembuh : " & loadDT.Rows(0).Item(2) & " Jiwa" & vbNewLine &
                   "Meninggal : " & loadDT.Rows(0).Item(3) & " Jiwa" & vbNewLine &
                   "Dirawat : " & loadDT.Rows(0).Item(4) & " Jiwa" & vbNewLine
        End Using
    End Function
    Private Sub calculatedataglobal()

        lbl_detailIndo.Text = viewIndo() 'Show Total Data indonesia
        '////Hitung Global Data
        lbl_positif.Text = totalpositif.calculategrid(DataGridView1, 4) & vbNewLine & "Jiwa" 'Hitung Total Positif (Column 4)
        lbl_sembuh.Text = totalsembuh.calculategrid(DataGridView1, 6) & vbNewLine & "Jiwa" 'Hitung Total Sembuh (Column 6)
        lbl_meninggal.Text = totalmeninggal.calculategrid(DataGridView1, 5) & vbNewLine & "Jiwa" 'Hitung Total Meninggal (Column 5)
    End Sub

#End Region
End Class